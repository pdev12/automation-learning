import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Listeners(ScreenShotListener.class)
public class AtinTest {

    @BeforeTest
    @Parameters({"geet", "person"})
    public void setUp(String browser, String personName) {
        System.out.println(browser);
        System.out.println(personName);
    }

    @Test
    public void tc1(){
        System.setProperty("webdriver.chrome.driver","/Users/atinsingh/Desktop/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("https://pragra.io");

        System.out.println("TC1");

    }

    @Test
    @Parameters("geet")
    public void tc2(String abc){
        System.out.println("TC1");
        System.out.println(abc);
    }

    @Test(groups = {"smoke","regression"})
    @Parameters("geet")
    public void tc3(String abc){
        System.out.println("TC1");
        System.out.println(abc);
    }


    @Test(dataProvider ="loginP2" , dataProviderClass = LoginProviders.class)
    public void loginTest(String user, int pass ){
        System.out.println("Got from Data Provider--> " + user + " --> " + pass);
    }



 }
