import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LoginProviders {
    @DataProvider(name = "users")
    public Object [] [] loginProvider(){
        String [] []  data = { {"vivek","pass"} , {"geet","abc123"} , { "parita","xyz234"} };
        return data;
    }

    @DataProvider
    public Iterator<Object[]> loginP2() {
        List<Object[]> data = new ArrayList<>();
        data.add(new Object[] {" Michakel", 39});
        data.add(new Object[] {" Adam", 48});
        data.add(new Object[] {" John", 93});
        return data.iterator();
    }
}
